#include <Keyboard.h>

#define DEBUG false

#define LAYERS 3
#define INS 6
#define OUTS 8
#define PSFT 256

//CODES
#define F1  194
#define F2  195
#define F3  196
#define F4  197
#define F5  198
#define F6  199
#define F7  200
#define F8  201
#define F9  202
#define F10 203
#define F11 204
#define F12 205

#define BSP 010
#define TAB 011
#define RET 012
#define ESC 177
#define DEL 212

#define SHF 129
#define CTR 128
#define ALT 130
#define OS  131
#define AGR 134

#define HOM 210
#define PUP 211
#define END 213
#define PDN 214

#define RIG 215
#define LEF 216
#define DOW 217
#define UP  218

#define LUP 256
#define LDN 257



byte in[INS] = {19, 18, 15, 14, 16, 10};
byte out[OUTS] = {2, 4, 6, 8, 3, 5, 7, 9};
bool keys[2][OUTS][INS];
byte layer = 1;
byte ktable = 0; // Controls which key table is being read to now
bool did_special = false;

unsigned int keymap[LAYERS][OUTS][INS] =
{{
    {'`'    ,'2'    ,000    ,000    ,'&'+256,000     },
    {ESC    ,'5'    ,'0'    ,'!'+256,'='    ,000     },
    {SHF    ,'8'    ,'.'+256,000    ,'('    ,']'     },
    {CTR    ,ALT    ,LUP    ,000    ,AGR    ,000     },
    {    '1'    ,'3'    ,000    ,'*'+256,'|'+256,BSP },
    {    '4'    ,'6'    ,000    ,'-'    ,000    ,'\\'},
    {    '7'    ,'9'    ,000    ,'['    ,')'    ,RET },
    {    000    ,OS     ,' '    ,LDN    ,000    ,000 },
},{
    {TAB    ,'w'    ,'r'    ,'y'    ,'i'    ,'p'     },
    {ESC    ,'s'    ,'f'    ,'h'    ,'k'    ,';'     },
    {SHF    ,'x'    ,'v'    ,'n'    ,','    ,'/'     },
    {CTR    ,ALT    ,LUP    ,000    ,AGR    ,000     },
    {    'q'    ,'e'    ,'t'    ,'u'    ,'o'    ,BSP },
    {    'a'    ,'d'    ,'g'    ,'j'    ,'l'    ,'\''},
    {    'z'    ,'c'    ,'b'    ,'m'    ,'.'    ,RET },
    {    000    ,OS    ,' '    ,LDN     ,000    ,000 },
},{
    {TAB    ,000    ,000    ,000    ,000    ,000     },
    {ESC    ,PUP    ,END    ,LEF    ,UP     ,000     },
    {SHF    ,000    ,000    ,000    ,000    ,000     },
    {CTR    ,ALT    ,LUP    ,000    ,AGR    ,000     },
    {    000    ,000    ,000    ,000    ,000    ,DEL },
    {    000    ,HOM    ,PDN    ,DOW    ,RIG    ,000 },
    {    000    ,000    ,000    ,000    ,000    ,RET },
    {    000    ,OS     ,' '    ,LDN    ,000    ,000 },
}};


void setup() {
  for (byte i = 0; i < OUTS; ++i) {
    pinMode(out[i], OUTPUT);
    digitalWrite(out[i], LOW);
  }
  for (byte i = 0; i < INS; ++i) {
    pinMode(in[i], INPUT);
  }
  
  Keyboard.begin();
}


void loop() {
  read_key_states();
  // DEBUG
  //show_table();
  //show_layer();
  // END DEBUG
  send_keystrokes();
  update_ktable();
  delayMicroseconds(500);
}


void read_key_states() {
  bool layerup = false;
  bool layerdown = false;

  for (int o = 0; o < OUTS; ++o) {
    digitalWrite(out[o], HIGH);
    delayMicroseconds(500);
    for (int i = 0; i < INS; ++i) {
      keys[ktable][o][i] = digitalRead(in[i]);
      if (keys[ktable][o][i] == 1 && keymap[layer][o][i] == PSFT) layerdown = true;
      if (keys[ktable][o][i] == 1 && keymap[layer][o][i] == PSFT+1) layerup = true;
    }
      digitalWrite(out[o], LOW);
  }

  if (layerup && layerdown) {
    change_layer(1);
    if (!did_special) {
      did_special = true;
      special_action();
    }
  } else if (layerup) {
    change_layer(2);
  } else if (layerdown) {
    change_layer(0);
  } else {
    change_layer(1);
    did_special = false;
  }
}

void send_keystrokes() {
  for (byte i = 0; i < INS; ++i) {
    for (byte o = 0; o < OUTS; ++o) {
      send_key(keys[1 - ktable][o][i], keys[ktable][o][i], keymap[layer][o][i]);
    }
  }
}

void send_key(bool old_state, bool new_state, int keycode) {
  if ((old_state || new_state) && keycode != 0) { 
    if (keycode > PSFT+1) {
      keycode -= PSFT;
      if (!old_state && new_state) {
        Keyboard.write(keycode);
        if (DEBUG) {
          Serial.print("single press: ");
          Serial.print(keycode);
          Serial.print('\n');
        }
      }
    } else if (keycode < PSFT) {
      if (old_state && !new_state) {
        Keyboard.release(keycode);
        if (DEBUG) {
          Serial.print("end press: ");
          Serial.print(keycode);
          Serial.print('\n');
        }
      } else if (!old_state && new_state) {
        Keyboard.press(keycode);
        if (DEBUG) {
          Serial.print("start press: ");
          Serial.print(keycode);
          Serial.print('\n');
        }
      }
    }
  }
}

void update_ktable() {
  ktable = 1 - ktable;
}

void change_layer(byte lyr) {
  if (lyr != layer) {
    for (byte i = 0; i < INS; ++i) {
      for (byte o = 0; o < OUTS; ++o) {
        if (keymap[layer][o][i] != keymap[lyr][o][i]
        && keys[ktable][o][i] == 1) {
          Keyboard.release(keymap[layer][o][i]);
        }
      }
    }
    layer = lyr;
  }
}

void special_action() {
  Keyboard.print("");
  if (DEBUG) {
    Serial.print("ASDASDASDASD\n");
  }
}

void show_table() {
  for (byte o = 0; o < OUTS; ++o) {
    for (byte i = 0; i < INS; ++i) {
      Serial.print(keys[ktable][o][i]);
      Serial.print(' ');
    }
    Serial.print('\n');
  }
}

void show_layer() {
  Serial.print("Layer: ");
  Serial.print(layer);
  Serial.print('\n');
}
