#include <Keyboard.h>

//#define DEBUG
//#define DEBUG_PRINT_TABLE

//CODES
#define F1   194
#define F2   195
#define F3   196
#define F4   197
#define F5   198
#define F6   199
#define F7   200
#define F8   201
#define F9   202
#define F10  203
#define F11  204
#define F12  205
#define F13  240
#define F14  241
#define F15  242
#define F16  243
#define F17  244
#define F18  245
#define F19  246
#define F20  247
#define F21  248
#define F22  249
#define F23  250
#define F24  251

#define BSPC 010
#define TAB  011
#define RET  012
#define ESC  177
#define INS  209
#define DEL  212

#define LSFT 129
#define RSFT 133
#define LCTR 128
#define RCTR 132
#define LALT 130
#define RALT 134
#define LGUI 131
#define RGUI 135
#define CPSL 193

#define HOME 210
#define PGUP 211
#define END  213
#define PGDN 214

#define RIGT 215
#define LEFT 216
#define DOWN 217
#define UP   218

#define LUP  256
#define LDWN 257

// KEYMAP MODIFIERS
// usage explained in KEYMAP section
#define H  0+    // [0   - 255] holdable press
#define P  256+  // [256 - 511] single press (single even if held)
#define D  512+  // [512 - 767] double function (if released quickly presses given key, if held holds key form dual function keymap)
#define LS 768+  // [768 - 783] shift to layer when holding
#define LH 784+  // [784 - 799] switch to layer
#define M  1000+ // [1000+    ] macro (pressing calls macro with given id)

#define H_OFFSET 0
#define P_OFFSET 256
#define D_OFFSET 512
#define LS_OFFSET 768
#define LH_OFFSET 784
#define M_OFFSET 1000



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                       CONFIGURATION                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// HARDWARE CONSTANTS
#define ROWS 5 // numer of output pins for selecting powered row
#define COLS 16 // number of columns (selected with mux)
#define MC 4 // number of mux control pins for selecting read column

// LAYOUT CONSTANTS
#define LAYERS 3 // number of keymap layers (16 MAX)

#define DUAL_DELAY 150 // time after which dual key is considered held (in ms)

// PINS
byte mc[MC] = {18, 19, 20, 21};
byte out[ROWS] = {5, 6, 7, 8, 9};
byte in = 10;

// KEYMAP
// mode followed by code from spcified range (whitespace separated)
//
// MODE
// || ARG_RANGE
// ||    ||      ARG_TYPE
// \/    \/         \/
// H  [0  -  255] - keycode - held down
// P  [0  -  255] - keycode - single press (preferred for symbols requiring shift
// D  [0  -  255] - keycode - dual function - sends keycode for single press, looks up in dual action keymap for held key
// LS [0  -  15 ] - layer number - switch to given layer
// LH [0  -  15 ] - layer number - shift to given layer
// M  [0 - 64735] - macro number - put them in the macros function in coresponding case branch


unsigned int keymap[LAYERS][ROWS][COLS] =
{ { // 0 default

    { H  '`'  , H  '1'  , H  '2'  , H  '3'  , H  '4'  , H  '5'  , H  LEFT , 0       , 0       , H  RIGT , H  '6'  , H  '7'  , H  '8'  , H  '9'  , H  '0'  , H  '-'  },

    { H  DEL  , H  'q'  , H  'w'  , H  'e'  , H  'r'  , H  't'  , H  '-'  , 0       , 0       , H  '='  , H  'y'  , H  'u'  , H  'i'  , H  'o'  , H  'p'  , H  '\\' },

    { H  ESC  , H  'a'  , H  's'  , H  'd'  , H  'f'  , H  'g'  , 0       , 0       , 0       , 0       , H  'h'  , H  'j'  , H  'k'  , H  'l'  , H  ';'  , D  '\'' },

    { H  LSFT , D  'z'  , H  'x'  , H  'c'  , H  'v'  , H  'b'  , 0       , H  HOME , H  PGUP , 0       , H  'n'  , H  'm'  , H  ','  , H  '.'  , D  '/'  , H  RSFT },

    { H  LCTR , LH 1    , H  LALT , H  LGUI , LH 1    , H  ' '  , H  BSPC , H  END  , H  PGDN , H  TAB  , H  RET  , LH 2    , H  RALT , H  '['  , H  ']'  , H  RCTR  },

  }, { // 1 symbols

    { 0       , H  F1   , H  F2   , H  F3   , H  F4   , H  F5   , H  LEFT , 0       , 0       , H  RIGT , H  F6   , H  F7   , H  F8   , H  F9   , H  F10  , H  F11  },

    { 0       , P  '!'  , P  '@'  , P  '{'  , P  '}'  , P  '!'  , 0       , 0       , 0       , 0       , H  '-'  , H  '7'  , H  '8'  , H  '9'  , P  '*'  , H  F12  },

    { 0       , P  '#'  , P  '$'  , P  '('  , P  ')'  , P  '&'  , 0       , 0       , 0       , 0       , 0       , H  '4'  , H  '5'  , H  '6'  , P  '+'  , H  '\'' },

    { H  LSFT , P  '%'  , P  '^'  , P  '['  , P  ']'  , P  '|'  , 0       , H  HOME , H  PGUP , 0       , 0       , H  '1'  , H  '2'  , H  '3'  , P  '\\' , H  RSFT },

    { H  LCTR , LH 1    , H  LALT , H  LGUI , LH 1    , H  ' '  , H  BSPC , H  END  , H  PGDN , H  TAB  , H  RET  , H  '0'  , 0       , 0       , H  '='  , H  RCTR },

  }, { // 2 navigation

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

    { H  ESC  , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , H  LEFT , H  DOWN , H  UP   , H RIGT  , 0       , 0       },

    { H  LSFT , 0       , 0       , 0       , 0       , 0       , 0       , H  HOME , H  PGUP , 0       , 0       , 0       , 0       , 0       , 0       , H  RSFT },

    { H  LCTR , 0       , H  LALT , H  LGUI , 0       , H  ' '  , H  DEL  , H  END  , H  PGDN , H  TAB  , H  RET  , LH 2    , 0       , 0       , H  '='  , H  RCTR },

  }
};


// can contain H and LH keys
unsigned int dual_keymap[LAYERS][ROWS][COLS] =
{ { // 0

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , H LGUI  },

    { 0       , H LCTR  , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , H RCTR  , 0       },

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

  }, { // 2

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

  }, { // 3

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

    { 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       , 0       },

  }
};

void macro(unsigned int id) {
  switch (id) {
    default:
      break;
  }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                     CONFIGURATION  END                                                                                              //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// GLOBAL VARIABLES
bool keys[2][ROWS][COLS]; // two tables of key press states, current and from pevious cycle
unsigned long last_press[ROWS][COLS]; //
byte base_layer = 0; // current base layer, switched with LS keys
byte shifted_layer = 0; // active layer, can differ from base_layer when LH key is pressed
byte ktable = 0; // Controls which key table is being read to now


void setup() {
  for (byte i = 0; i < ROWS; ++i) {
    pinMode(out[i], OUTPUT);
    digitalWrite(out[i], LOW);
  }
  for (byte i = 0; i < MC; ++i) {
    pinMode(mc[i], OUTPUT);
  }
  pinMode(in, INPUT);

  Keyboard.begin();
}

unsigned long last_loop = 0;

void loop() {
  unsigned long last_loop_tmp = last_loop;
  last_loop = micros();
  update_key_states();
#ifdef DEBUG_PRINT_TABLE
  show_table();
#endif
#ifdef DEBUG
  show_layer();
#endif
  send_keystrokes();
  switch_ktable();

  delay(4);
}


void set_input(int col) {
  if (col % 2 == 1) digitalWrite(mc[0], HIGH);
  else digitalWrite(mc[0], LOW);
  if ((col / 2) % 2 == 1) digitalWrite(mc[1], HIGH);
  else digitalWrite(mc[1], LOW);
  if ((col / 4) % 2 == 1) digitalWrite(mc[2], HIGH);
  else digitalWrite(mc[2], LOW);
  if ((col / 8) % 2  == 1) digitalWrite(mc[3], HIGH);
  else digitalWrite(mc[3], LOW);
  delayMicroseconds(3);
}

// TODO add layer switch behind Dual keys support
void update_key_states() {
  byte new_shifted_layer = shifted_layer;
  bool layer_shift_pressed = false;

  for (int o = 0; o < ROWS; ++o) {
    digitalWrite(out[o], HIGH);
    delayMicroseconds(3);
    for (int i = 0; i < COLS; ++i) {
      set_input(i);
      keys[ktable][o][i] = digitalRead(in);
      if (keys[ktable][o][i] == 1 && keys[1 - ktable][o][i] == 0
          && keymap[shifted_layer][o][i] >= LS_OFFSET && keymap[shifted_layer][o][i] < LS_OFFSET + 16)
      {
        base_layer = keymap[shifted_layer][o][i] - LS_OFFSET;
      }
      else if (keys[ktable][o][i] == 1
               && keymap[shifted_layer][o][i] >= LH_OFFSET && keymap[shifted_layer][o][i] < LH_OFFSET + 16)
      {
        layer_shift_pressed = true;
        new_shifted_layer = keymap[shifted_layer][o][i] - LH_OFFSET;
      }
    }
    digitalWrite(out[o], LOW);
  }

  if (layer_shift_pressed) {
    change_layer(new_shifted_layer);
  } else {
    change_layer(base_layer);
  }
}

void send_keystrokes() {
  for (byte i = 0; i < COLS; ++i) {
    for (byte o = 0; o < ROWS; ++o) {
      send_key(i, o, keys[1 - ktable][o][i], keys[ktable][o][i]);
    }
  }
}

// TODO add dual function keys support
void send_key(byte col, byte row, bool old_state, bool new_state) {
  unsigned int keycode = keymap[shifted_layer][row][col];
  if (keycode == 0) return;
  if (keycode < H_OFFSET + 256) { // Hold keys
    if (!old_state && new_state) {
      Keyboard.press(keycode - H_OFFSET);
    }
    if (old_state && !new_state) {
      Keyboard.release(keycode - H_OFFSET);
    }
  } else if (keycode < P_OFFSET + 256) { // Press keys
    if (!old_state && new_state) {
      Keyboard.write(keycode - P_OFFSET);
    }
  } else if (keycode < D_OFFSET + 256) { // Dual keys
    if (!old_state && new_state) {
      last_press[row][col] = millis();
    }
    if (old_state && new_state && millis() - last_press[row][col] >= DUAL_DELAY) {
      if (dual_keymap[shifted_layer][row][col] < 256) {
        Keyboard.press(dual_keymap[shifted_layer][row][col] - H_OFFSET);
      }
    }
    if (old_state && !new_state && millis() - last_press[row][col] >= DUAL_DELAY) {
      if (dual_keymap[shifted_layer][row][col] < 256) {
        Keyboard.release(dual_keymap[shifted_layer][row][col] - H_OFFSET);
      }
    }
    if (old_state && !new_state && millis() - last_press[row][col] < DUAL_DELAY) {
      Keyboard.write(keycode - D_OFFSET);
    }
  } else if (keycode >= M_OFFSET) { // Macro keys
    if (!old_state && new_state) {
      macro(keycode - M_OFFSET);
    }
  }
}

void switch_ktable() {
  ktable = 1 - ktable;
}

void change_layer(byte lyr) {
  if (lyr != shifted_layer && lyr >= 0 && lyr < LAYERS) {
    for (byte i = 0; i < COLS; ++i) {
      for (byte o = 0; o < ROWS; ++o) {
        if (keymap[shifted_layer][o][i] != keymap[lyr][o][i]
            && keys[ktable][o][i] == 1) {
          Keyboard.release(keymap[shifted_layer][o][i]);
        }
      }
    }
    shifted_layer = lyr;
  }
}


// DEBUG STUFF

void show_table() {
  for (byte o = 0; o < ROWS; ++o) {
    for (byte i = 0; i < COLS; ++i) {
      Serial.print(keys[ktable][o][i]);
      Serial.print(' ');
    }
    Serial.print('\n');
  }
}

void show_layer() {
  Serial.print("Layer: ");
  Serial.print(shifted_layer);
  Serial.print('\n');
}
